# Project_Spring2023
RULES OF GROUP

4 NĂM VẤT VẢ -- HÁI QUẢ 1 NGÀY

Vì suốt 4 năm qua mọi người đã cố gắng rất nhiều rồi cho nên với tư cách là Team Lead, thì mình mong muốn mọi người sẽ cùng nhau cố gắng hơn nửa, áp lực hơn nửa trong khoảng thời gian ít ỏi còn lại để mình hái quả vào ngày quyết định nhé. Chính vì thế mình sẽ khắc khe hơn nhé mọi người vì một kết quả tốt đẹp cho tất cả chứ không của riêng ai. Mong mọi người thông cảm. Mình sẽ gắn bó với nhau suốt 7 tháng liền, cho nên chung ta cũng phải có nhưng qui luật chung để dễ làm việc.


1. Hoạt động nhóm.
	+ Luôn Luôn phải bật thông báo tin nhắn nhóm.
	+ Theo dõi/ đặt lịch hẹn theo những buổi họp của nhóm.
	+ Luôn theo ý kiến số đông.
	+ Mỗi người đều phải đóng góp ý kiến (Đồ án của cả 4 người không của riêng ai).
	+ Nếu lịch hẹn trùng với công chuyện riêng phải thông báo trước. Không được đợi đến sát hẹn mới thông báo.
	+ LƯU Ý: Bất kể tin nhắn của ai trong group mình luôn phải có sự phản hồi (có thể like).
	+ Đưa ra 1 lịch hoạt động cụ thể mỗi tuần của cả nhóm để tổng hợp cũng như báo cáo tiến độ.

2. Về Việc trao đổi với thầy:
	+ Bất kể khi nào thầy thông báo/ gửi tài liệu/ đưa yêu cầu tất cả thành viên phải phản hồi thầy. Đây là phép lịch sự tổi thiểu, đồng thời cũng thể hiện sự biết ơn, vui vẻ hòa đồng với thầy sẽ khiên thầy tích cực thoải mái với mình hơn.
	+ Đặt với thầy càng nhiều câu hỏi thì requirment càng rõ ràng mình sẽ dễ dàng hiểu vấn đề hơn. khi trả lời trước hội đồng mình sẽ tự tin hơn. Đừng ngại hỏi thầy.

3. Lịch làm việc nhóm
	+ Thứ Sáu hàng tuần sẽ là ngày họp của cả nhóm để tổng hợp cũng như báo cáo tiến độ.
	+ Tùy vào giai đoạn khác nhau sẽ có mật độ khác nhau

4. Cách đặt tên file
	+ Cách đặt tên biến: kiểu Camel (binhNuoc)
	+ Cách đặt tên class: viết hoa chữ đầu (TenClass)
	+ Cách đặt tên package: tên dự án + viết thường chữ đầu (ww.tenpackage)
	+ Cách đặt tên file: WeWater_tênfile_version.typeFile
