Tạm thời sẽ có 4 actor chính trong hệ thông:

	+ Admin (Hưng)
		
	+ Employee for warehouse (Hiển)

	+ Shipper (Vũ)

	+ Custommer  
		- For first-time login (Hưng)
		- For second-time login (Hoàng)
		- Order (Hoàng)
Dựa vào ERD và Context để làm bussiness rule 

VD: 
	WW-01 Admin Employee Shipper login by username and password
	WW-02 Customer login by PhoneNumber and Password, or QR-code
	WW-03 Customer only have one QR-code.
	WW-04 Only admin can create an employee account, shipper account
	WW-05 Employee can view list of orders
	WW-06 Employee will create order for shippers
	WW-07 Shipper can view order by Employee
	WW-08	Shipper can choose accept or reject order
	WW-09 Shipper can get more than one order if area of those orders are the same
	WW-10 Customer can create many orders
	WW-11 After Customer use QR-code for login, dialog for booking immediately will be shown. 

Nói chung liệt kê tất cả những chức năng có trong hệ thống (nghĩ được chức năng gì ghi đó với điều kiện ghi rõ từng bước ghi rõ những thuộc tính thay đổi sau khi thực hiện chức nănng nếu có), các mối quan hệ trong ERD (ví dụ WW-03, WW-10). MỌI NGƯỜI THAM KHẢO FILE ĐỒ ÁN KỲ TRƯỚC ĐỂ LÀM. Liệt kê càng nhiều càng tốt vì phải còn chỉnh sửa bổ sung chọn lọc nhiều lần. LÀM TRONG FILE EXCEL, NHỚ ĐÁNH MÃ CHO TỪNG RULE WW-XX (WW là WeWater, XX là số ) Liệt kê theo thứ tự trong ERD và CONTEXT để không bị sót
